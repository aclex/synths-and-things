Ebuild files for various music creation related packages
==========

The project contains a portage directory subtree containing various nice and useful instruments, tools and programs related to music creation and producing, ready to be built and merged in Gentoo-based distros.

How to use
---------

### Current repository setup

Add our current portage repository to the system: 

```bash
cat >> /etc/portage/repos.conf/synths-and-things.conf << EOF
[synths-and-things]
location = /var/db/repos/synths-and-things
sync-type = git
sync-uri = https://gitlab.com/aclex/synths-and-things
auto-sync = yes
EOF
```

Afterwards you just sync the changes (`emerge --sync`, `eix-sync` etc.) and merge the packages using the common utilities via e.g. `emerge`.

What's inside
--------

* [x] [`geonkick`](https://gitlab.com/iurie-sw/geonkick) kick synthesizer

