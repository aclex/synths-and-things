# Copyright 2020 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit toolchain-funcs multilib


DESCRIPTION="Bundle of free audio effects"
HOMEPAGE="https://github.com/michaelwillis/${PN}"
SRC_URI="${HOMEPAGE}/archive/${PV}/${PN}-${PV}.tar.gz -> ${P}.tar.gz
https://github.com/DISTRHO/DPF/archive/dc6557a3c935ae4bb68f89d8bc698a805fff788e.tar.gz -> DPF-dc6557a3c935ae4bb68f89d8bc698a805fff788e.tar.gz
https://github.com/DISTRHO/pugl/archive/844528e197c51603f6cef3238b4a48d23bf60eb7.tar.gz -> pugl-844528e197c51603f6cef3238b4a48d23bf60eb7.tar.gz
"

LICENSE="GPL-3"
SLOT="0/${PV}"
KEYWORDS="~amd64 ~arm64"
IUSE="clap lv2 vst standalone"

DEPEND="
	virtual/opengl
	media-libs/libsamplerate
	standalone? ( virtual/jack )
	lv2? ( media-libs/lv2 )
	>=media-libs/freeverb3-3.2.1
"

src_prepare() {
	default

	rmdir dpf || die
	ln -sv "${WORKDIR}"/DPF-dc6557a3c935ae4bb68f89d8bc698a805fff788e dpf || die

	rmdir dpf/dgl/src/pugl-upstream || die
	ln -sv "${WORKDIR}"/pugl-844528e197c51603f6cef3238b4a48d23bf60eb7 dpf/dgl/src/pugl-upstream || die

	rm -r common/freeverb || die

	sed -i "s/ \-Wl\,\-\-strip\-all//g" dpf/Makefile.base.mk || die

	targets=()

	if use clap; then
		targets+=(clap)
	fi

	if use standalone; then
		targets+=(jack)
	fi

	if use lv2; then
		targets+=(lv2_sep)
	fi

	if use vst; then
		targets+=(vst3)
	fi

	targets="${targets[@]}"
	echo "targets: ${targets}"

	find plugins -name Makefile -exec sed -i "s/TARGETS = .*/TARGETS = ${targets}/" {} \; || die
}

src_compile() {
	emake CC="$(tc-getCC)" SYSTEM_FREEVERB3="true"
}

src_install() {
	pushd "${BUILD_DIR}" > /dev/null || die

	if use standalone; then
		mkdir -pv "${D}/usr/bin" || die
		cp -av "bin/DragonflyEarlyReflections" \
			"bin/DragonflyHallReverb" \
			"bin/DragonflyPlateReverb" \
			"bin/DragonflyRoomReverb" \
			"${D}/usr/bin" || die
	fi

	if use clap; then
		mkdir -pv "${D}/usr/$(get_libdir)/clap" || die
		cp -av "bin/DragonflyEarlyReflections.clap" \
			"bin/DragonflyHallReverb.clap" \
			"bin/DragonflyPlateReverb.clap" \
			"bin/DragonflyRoomReverb.clap" \
			"${D}/usr/$(get_libdir)/clap" || die
	fi

	if use lv2; then
		mkdir -pv "${D}/usr/$(get_libdir)/lv2" || die
		cp -av "bin/DragonflyEarlyReflections.lv2" \
			"bin/DragonflyHallReverb.lv2" \
			"bin/DragonflyPlateReverb.lv2" \
			"bin/DragonflyRoomReverb.lv2" \
			"${D}/usr/$(get_libdir)/lv2" || die
	fi

	if use vst; then
		mkdir -pv "${D}/usr/$(get_libdir)/vst3" || die
		cp -av "bin/DragonflyEarlyReflections.vst3" \
			"bin/DragonflyHallReverb.vst3" \
			"bin/DragonflyPlateReverb.vst3" \
			"bin/DragonflyRoomReverb.vst3" \
			"${D}/usr/$(get_libdir)/vst3" || die
	fi

	popd > /dev/null || die
}
