# Copyright 2020 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit git-r3 toolchain-funcs multilib


DESCRIPTION="Bundle of free reverb audio effects"
HOMEPAGE="https://github.com/michaelwillis/${PN}"
SRC_URI="${HOMEPAGE}/archive/${PV}/${PN}-${PV}.tar.gz -> ${P}.tar.gz"

EGIT_REPO_URI="${HOMEPAGE}"
EGIT_COMMIT="${PV}"
EGIT_SUBMODULES=('*')

LICENSE="GPL-3"
SLOT="0/${PV}"
KEYWORDS="~amd64 ~arm64"
IUSE=""

DEPEND="
	virtual/opengl
	media-libs/libsamplerate
	virtual/jack
	media-libs/lv2
"

src_prepare() {
	default

	sed -i "s/ \-Wl\,\-\-strip\-all//g" common/Makefile.mk || die
	sed -i "s/ \-Wl\,\-\-strip\-all//g" dpf/Makefile.base.mk || die
}

src_compile() {
	emake CC="$(tc-getCC)"
}

src_install() {
	pushd "${BUILD_DIR}" > /dev/null || die

	mkdir -pv "${D}/usr/bin" || die
	cp -av "bin/DragonflyEarlyReflections" \
		"bin/DragonflyHallReverb" \
		"bin/DragonflyPlateReverb" \
		"bin/DragonflyRoomReverb" \
		"${D}/usr/bin" || die

	mkdir -pv "${D}/usr/$(get_libdir)/lv2" || die
	cp -av "bin/DragonflyEarlyReflections.lv2" \
		"bin/DragonflyHallReverb.lv2" \
		"bin/DragonflyPlateReverb.lv2" \
		"bin/DragonflyRoomReverb.lv2" \
		"${D}/usr/$(get_libdir)/lv2" || die

	mkdir -pv "${D}/usr/$(get_libdir)/vst" || die
	cp -av "bin/DragonflyEarlyReflections-vst.so" \
		"bin/DragonflyHallReverb-vst.so" \
		"bin/DragonflyPlateReverb-vst.so" \
		"bin/DragonflyRoomReverb-vst.so" \
		"${D}/usr/$(get_libdir)/vst" || die

	popd > /dev/null || die
}
