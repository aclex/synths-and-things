# Copyright 2020 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit toolchain-funcs multilib


DESCRIPTION="Bundle of free audio effects"
HOMEPAGE="https://github.com/michaelwillis/${PN}"
SRC_URI="${HOMEPAGE}/archive/${PV}/${PN}-${PV}.tar.gz -> ${P}.tar.gz
https://github.com/DISTRHO/DPF/archive/22413340a6d8ef2ffbf38ce841fb44c448a1a84a.tar.gz -> DPF-22413340a6d8ef2ffbf38ce841fb44c448a1a84a.tar.gz
https://github.com/DISTRHO/pugl/archive/3e03459a5a0b0f118b04e9e0b0a32f42ccd04a5c.tar.gz -> pugl-3e03459a5a0b0f118b04e9e0b0a32f42ccd04a5c.tar.gz
"

LICENSE="GPL-3"
SLOT="0/${PV}"
KEYWORDS="~amd64 ~arm64"
IUSE="lv2 vst standalone"

DEPEND="
	virtual/opengl
	media-libs/libsamplerate
	standalone? ( virtual/jack )
	lv2? ( media-libs/lv2 )
	>=media-libs/freeverb3-3.2.1
"

src_prepare() {
	default

	rmdir dpf || die
	ln -sv "${WORKDIR}"/DPF-22413340a6d8ef2ffbf38ce841fb44c448a1a84a dpf || die

	rmdir dpf/dgl/src/pugl-upstream || die
	ln -sv "${WORKDIR}"/pugl-3e03459a5a0b0f118b04e9e0b0a32f42ccd04a5c dpf/dgl/src/pugl-upstream || die

	rm -r common/freeverb || die

	sed -i "s/ \-Wl\,\-\-strip\-all//g" dpf/Makefile.base.mk || die

	targets=()

	if use standalone; then
		targets+=(jack)
	fi

	if use lv2; then
		targets+=(lv2_sep)
	fi

	if use vst; then
		targets+=(vst3)
	fi

	targets="${targets[@]}"
	echo "targets: ${targets}"

	find plugins -name Makefile -exec sed -i "s/TARGETS = .*/TARGETS = ${targets}/" {} \; || die
}

src_compile() {
	emake CC="$(tc-getCC)" SYSTEM_FREEVERB3="true"
}

src_install() {
	pushd "${BUILD_DIR}" > /dev/null || die

	if use standalone; then
		mkdir -pv "${D}/usr/bin" || die
		cp -av "bin/DragonflyEarlyReflections" \
			"bin/DragonflyHallReverb" \
			"bin/DragonflyPlateReverb" \
			"bin/DragonflyRoomReverb" \
			"${D}/usr/bin" || die
	fi

	if use lv2; then
		mkdir -pv "${D}/usr/$(get_libdir)/lv2" || die
		cp -av "bin/DragonflyEarlyReflections.lv2" \
			"bin/DragonflyHallReverb.lv2" \
			"bin/DragonflyPlateReverb.lv2" \
			"bin/DragonflyRoomReverb.lv2" \
			"${D}/usr/$(get_libdir)/lv2" || die
	fi

	if use vst; then
		mkdir -pv "${D}/usr/$(get_libdir)/vst3" || die
		cp -av "bin/DragonflyEarlyReflections.vst3" \
			"bin/DragonflyHallReverb.vst3" \
			"bin/DragonflyPlateReverb.vst3" \
			"bin/DragonflyRoomReverb.vst3" \
			"${D}/usr/$(get_libdir)/vst3" || die
	fi

	popd > /dev/null || die
}
