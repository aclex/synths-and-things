# Copyright 2025 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit cmake

DESCRIPTION="Free & open source hybrid synthesizer"
HOMEPAGE="https://surge-synthesizer.github.io"

TAG_SHORT="f7b97c68"

CLAP_TAG="df8f16c69ba1c1a15fb105f0c5a2e5b9ac6be742"
CLAP_HELPERS_TAG="7b53a685e11465154b4ccba3065224dbcbf8a893"
CLAP_JUCE_EXTENSIONS_TAG="087dd16e2d17e6b3f1c9a92817a8a7348ea08168"
EURORACK_TAG="f7614460cd3e295f6f6132f9052984c7bca9a69d"
JUCE_TAG="cf5754b19c87ea63758802e3f4239c05a77f1412"
MELATONIN_INSPECTOR_TAG="b7a0f2de07ed8692963605561fe1a3793885994b"
MTS_ESP_TAG="3eb56f2452244b343ce738efd067d1082b67f9b4"
PEGTL_TAG="64af78c6a7959cd5753ad165ec9f65591aa96f2d"
PFFFT_TAG="96292e6965f74d5e08108f5f717df0bf8aa06b57"
SST_BASIC_BLOCKS_TAG="ae9c7e4f3ab58c9b26c5b45995a8adc16ec0830d"
SST_CPPUTILS_TAG="9418be4d6e059e236db43f99925f46492203ec55"
SST_EFFECTS_TAG="43a54d94370986f7fe92931493435f2a8b67603f"
SST_FILTERS_TAG="9eb74b595c0fc244fdd67452fc17948edec4cf28"
SST_PLUGININFRA_TAG="49ee7eb761bc06d0c0a012659f975d59b868e3e8"
SST_WAVESHAPERS_TAG="42bfa678e4f2447d0f4c9a51b7ba7ea68f6da108"
TUNING_LIBRARY_TAG="4cbe55f2da86c0302368a681406553a1dd7074dd"

SRC_URI="https://github.com/surge-synthesizer/surge/archive/release-xt/${PV}.tar.gz -> ${P}.tar.gz
clap? ( https://github.com/free-audio/clap/archive/${CLAP_TAG}.tar.gz -> clap-${CLAP_TAG}.tar.gz
https://github.com/free-audio/clap-helpers/archive/${CLAP_HELPERS_TAG}.tar.gz -> clap-helpers-${CLAP_HELPERS_TAG}.tar.gz
https://github.com/free-audio/clap-juce-extensions/archive/${CLAP_JUCE_EXTENSIONS_TAG}.tar.gz -> clap-juce-extensions-${CLAP_JUCE_EXTENSIONS_TAG}.tar.gz )
https://github.com/surge-synthesizer/JUCE/archive/${JUCE_TAG}.tar.gz -> juce-${JUCE_TAG}.tar.gz
rack? ( https://github.com/sudara/melatonin_inspector/archive/${MELATONIN_INSPECTOR_TAG}.tar.gz -> melatonin_inspector-${MELATONIN_INSPECTOR_TAG}.tar.gz
https://github.com/surge-synthesizer/eurorack/archive/${EURORACK_TAG}.tar.gz -> eurorack-${EURORACK_TAG}.tar.gz )
https://github.com/ODDSound/MTS-ESP/archive/${MTS_ESP_TAG}.tar.gz -> MTS-ESP-${MTS_ESP_TAG}.tar.gz
https://github.com/taocpp/PEGTL/archive/${PEGTL_TAG}.tar.gz -> PEGTL-${PEGTL_TAG}.tar.gz
https://github.com/surge-synthesizer/pffft/archive/${PFFFT_TAG}.tar.gz -> pffft-${PFFFT_TAG}.tar.gz
https://github.com/surge-synthesizer/sst-basic-blocks/archive/${SST_BASIC_BLOCKS_TAG}.tar.gz -> sst-basic-blocks.tar.gz
https://github.com/surge-synthesizer/sst-cpputils/archive/${SST_CPPUTILS_TAG}.tar.gz -> sst-cpputils.tar.gz
https://github.com/surge-synthesizer/sst-effects/archive/${SST_EFFECTS_TAG}.tar.gz -> sst-effects.tar.gz
https://github.com/surge-synthesizer/sst-filters/archive/${SST_FILTERS_TAG}.tar.gz -> sst-filters.tar.gz
https://github.com/surge-synthesizer/sst-plugininfra/archive/${SST_PLUGININFRA_TAG}.tar.gz -> sst-plugininfra.tar.gz
https://github.com/surge-synthesizer/sst-waveshapers/archive/${SST_WAVESHAPERS_TAG}.tar.gz -> sst-waveshapers.tar.gz
https://github.com/surge-synthesizer/tuning-library/archive/${TUNING_LIBRARY_TAG}.tar.gz -> tuning-library-${TUNING_LIBRARY_TAG}.tar.gz
"

LICENSE="GPL-3"
SLOT="1/${PV}"
KEYWORDS="~amd64 ~arm64"
IUSE="alsa clap fx +lv2 lua python rack standalone test vst3 xt"

BDEPEND="virtual/pkgconfig"

DEPEND="
	test? ( dev-cpp/catch )
	dev-cpp/cli11
	dev-db/sqlite
	lua? ( dev-lang/luajit )
	dev-libs/libfmt
	dev-libs/simde
	dev-libs/tinyxml
	python? ( dev-python/pybind11 )
	alsa? ( media-libs/alsa-lib )
	media-libs/freetype
	media-libs/libsamplerate
	lv2? ( media-libs/lv2 )
	standalone? ( virtual/jack )
"

PATCHES=(
	"${FILESDIR}/${PV}/Depend-on-system-libraries-where-possible.patch"
	"${FILESDIR}/${PV}/Include-system-tinyxml-and-use-platform-filesystem.patch"
)

src_unpack() {
	default

	mv "${WORKDIR}/surge-release-xt-${PV}" "${S}" || die

	cp -a "${FILESDIR}/${PV}/lj_arch.h" "${S}/src/common" || die
	cp -a "${FILESDIR}/${PV}/VERSION_GIT_INFO" "${S}" || die

	if use clap; then
		mv -T "${WORKDIR}/clap-juce-extensions-${CLAP_JUCE_EXTENSIONS_TAG}" "${S}/libs/clap-juce-extensions" || die
		mv -T "${WORKDIR}/clap-${CLAP_TAG}" "${S}/libs/clap-juce-extensions/clap-libs/clap" || die
		mv -T "${WORKDIR}/clap-helpers-${CLAP_HELPERS_TAG}" "${S}/libs/clap-juce-extensions/clap-libs/clap-helpers" || die
	fi

    mv -T "${WORKDIR}/JUCE-${JUCE_TAG}" "${S}/libs/JUCE" || die

	if use rack; then
		mv -T "${WORKDIR}/melatonin_inspector-${MELATONIN_INSPECTOR_TAG}" "${S}/libs/melatonin_inspector" || die
		mv -T "${WORKDIR}/eurorack-${EURORACK_TAG}" "${S}/libs/eurorack/eurorack" || die
	fi

	mv -T "${WORKDIR}/MTS-ESP-${MTS_ESP_TAG}" "${S}/libs/oddsound-mts/MTS-ESP" || die
	mv -T "${WORKDIR}/PEGTL-${PEGTL_TAG}" "${S}/libs/PEGTL" || die
    mv -T "${WORKDIR}/pffft-${PFFFT_TAG}" "${S}/libs/pffft" || die

    mv -T "${WORKDIR}/sst-basic-blocks-${SST_BASIC_BLOCKS_TAG}" "${S}/libs/sst/sst-basic-blocks" || die
    mv -T "${WORKDIR}/sst-cpputils-${SST_CPPUTILS_TAG}" "${S}/libs/sst/sst-cpputils" || die
    mv -T "${WORKDIR}/sst-effects-${SST_EFFECTS_TAG}" "${S}/libs/sst/sst-effects" || die
    mv -T "${WORKDIR}/sst-filters-${SST_FILTERS_TAG}" "${S}/libs/sst/sst-filters" || die
    mv -T "${WORKDIR}/sst-plugininfra-${SST_PLUGININFRA_TAG}" "${S}/libs/sst/sst-plugininfra" || die
    mv -T "${WORKDIR}/sst-waveshapers-${SST_WAVESHAPERS_TAG}" "${S}/libs/sst/sst-waveshapers" || die

	mv -T "${WORKDIR}/tuning-library-${TUNING_LIBRARY_TAG}" "${S}/libs/tuning-library" || die

	rm -rf "${S}/libs/CLI11" || die
	rm -rf "${S}/libs/LuaJitLib" || die
	rm -rf "${S}/libs/catch2_v3" || die
	rm -rf "${S}/libs/fmt" || die
	rm -rf "${S}/libs/libpng" || die
	rm -rf "${S}/libs/libsamplerate" || die
	rm -rf "${S}/libs/pybind11" || die
	rm -rf "${S}/libs/simde" || die
	rm -rf "${S}/libs/sqlite-3.23.3" || die
}

src_configure() {
	local mycmakeargs=(
		-DBUILD_SHARED_LIBS=OFF
		-DSURGE_BUILD_CLAP=$(usex clap ON OFF)
		-DSURGE_BUILD_FX=$(usex fx ON OFF)
		-DSURGE_BUILD_PYTHON_BINDINGS=$(usex python ON OFF)
		-DSURGE_SKIP_ALSA=$(usex alsa OFF ON)
		-DSURGE_SKIP_JUCE_FOR_RACK=$(usex rack OFF ON)
		-DSURGE_SKIP_LUA=$(usex lua OFF ON)
		-DSURGE_SKIP_STANDALONE=$(usex standalone OFF ON)
		-DSURGE_SKIP_VST3=$(usex vst3 OFF ON)
		-DSURGE_BUILD_LV2=$(usex lv2 ON OFF)
		-DSURGE_BUILD_XT=$(usex xt ON OFF)
		-DSURGE_BUILD_TESTRUNNER=$(usex test ON OFF)
		-DSST_PLUGININFRA_FILESYSTEM_FORCE_PLATFORM=ON
		-DSST_PLUGININFRA_PROVIDE_TINYXML=OFF
		-DSST_PLUGININFRA_PROVIDE_MINIZ=OFF
	)

	cmake_src_configure
}

src_install() {
	pushd "${BUILD_DIR}" > /dev/null || die

	if use lv2 && (use xt || use fx); then
		PLUGIN_DIR="${D}/usr/$(get_libdir)/lv2"
		mkdir -pv ${PLUGIN_DIR} || die
		if use fx; then
			cp -av "surge_xt_products/Surge XT Effects.lv2" ${PLUGIN_DIR} || die
		fi
		if use xt; then
			cp -av "surge_xt_products/Surge XT.lv2" ${PLUGIN_DIR} || die
		fi
	fi

	if use vst3 && (use xt || use fx); then
		PLUGIN_DIR="${D}/usr/$(get_libdir)/vst3"
		mkdir -pv ${PLUGIN_DIR} || die
		if use fx; then
			cp -av "surge_xt_products/Surge XT Effects.vst3" ${PLUGIN_DIR} || die
		fi
		if use xt; then
			cp -av "surge_xt_products/Surge XT.vst3" ${PLUGIN_DIR} || die
		fi
	fi

	if use clap && (use xt || use fx); then
		PLUGIN_DIR="${D}/usr/$(get_libdir)/clap"
		mkdir -pv ${PLUGIN_DIR} || die
		if use fx; then
			cp -av "surge_xt_products/Surge XT Effects.clap" ${PLUGIN_DIR} || die
		fi
		if use xt; then
			cp -av "surge_xt_products/Surge XT.clap" ${PLUGIN_DIR} || die
		fi
	fi

	if use standalone && (use xt || use fx); then
		BIN_DIR="${D}/usr/bin"
		mkdir -pv ${BIN_DIR} || die
		if use fx; then
			cp -av "surge_xt_products/Surge XT Effects" ${BIN_DIR} || die
		fi
		if use xt; then
			cp -av "surge_xt_products/Surge XT" ${BIN_DIR} || die
		fi
	fi

	popd > /dev/null || die

	pushd "${S}" > /dev/null || die
	einstalldocs
	popd > /dev/null || die
}
