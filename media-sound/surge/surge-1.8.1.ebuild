# Copyright 2020 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit cmake git-r3 multilib

DESCRIPTION="Free & open source hybrid synthesizer"
HOMEPAGE="https://surge-synthesizer.github.io"
SRC_URI="https://github.com/surge-synthesizer/surge/archive/release_${PV}/${PN}-release_${PV}.tar.gz -> ${P}.tar.gz"

EGIT_REPO_URI="https://github.com/surge-synthesizer/${PN}"
EGIT_COMMIT="release_${PV}"
EGIT_SUBMODULES=('*')

LICENSE="GPL-3"
SLOT="0/${PV}"
KEYWORDS="~amd64 ~arm64"
IUSE="+lv2 headless vst3"

DEPEND="
	x11-libs/cairo
	media-libs/fontconfig
	x11-libs/libX11
	x11-libs/libxcb[xkb]
	x11-libs/xcb-util-cursor
	x11-libs/xcb-util-keysyms
	x11-libs/xcb-util
	x11-libs/libxkbcommon[X]
	lv2? ( media-libs/lv2 )
"

src_configure() {
	local mycmakeargs=(
		-DBUILD_VST3=$(usex vst3 ON OFF)
		-DBUILD_VST2=OFF
		-DBUILD_LV2=$(usex lv2 ON OFF)
		-DBUILD_HEADLESS=$(usex headless ON OFF)
		-DBUILD_SURGE_PYTHON_BINDINGS=OFF
		-DBUILD_SHARED_LIBS=OFF
	)

	cmake_src_configure
}

src_install() {
	pushd "${BUILD_DIR}" > /dev/null || die
	mkdir -pv "${D}/usr/share/surge" || die
	cp -av "${S}/resources/data" "${D}/usr/share/surge" || die
	if use lv2; then
		mkdir -pv "${D}/usr/$(get_libdir)/lv2" || die
		cp -av "surge_products/Surge.lv2" "${D}/usr/$(get_libdir)/lv2" || die
	fi

	if use vst3; then
		mkdir -pv "${D}/usr/$(get_libdir)/vst3" || die
		cp -av "surge_products/Surge.vst3" "${D}/usr/$(get_libdir)/vst3" || die
	fi

	popd > /dev/null || die

	pushd "${S}" > /dev/null || die
	einstalldocs
	popd > /dev/null || die
}
