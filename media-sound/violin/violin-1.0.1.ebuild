# Copyright 2020 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit cmake git-r3

DESCRIPTION="Virtual instrument wrapper for GNU/Linux"
HOMEPAGE="https://gitlab.com/aclex/violin"
SRC_URI="${HOMEPAGE}/-/archive/${PV}/${PN}-${PV}.tar.gz -> ${P}.tar.gz"

EGIT_REPO_URI=${HOMEPAGE}
EGIT_COMMIT="${PV}"


LICENSE="GPL-3"
SLOT="0/${PV}"
KEYWORDS="~amd64 ~x86 ~arm ~arm64"

DEPEND=">=app-emulation/wine-vanilla-4[threads]"

BDEPEND="virtual/pkgconfig"
