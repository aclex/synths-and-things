# Copyright 2020 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit cmake

DESCRIPTION="A percussive synthesizer"
HOMEPAGE="https://geonkick.org/"
REPO_URL="https://codeberg.org/Geonkick-Synthesizer/geonkick"
VST3SDK_TAG="3.7.12_build_20"
SRC_URI="${REPO_URL}/archive/v${PV}.tar.gz -> ${P}.tar.gz
vst? ( https://github.com/steinbergmedia/vst3sdk/archive/refs/tags/v${VST3SDK_TAG}.tar.gz -> vst3sdk-${VST3SDK_TAG}.tar.gz
https://github.com/steinbergmedia/vst3_base/archive/refs/tags/v${VST3SDK_TAG}.tar.gz -> vst3sdk-base-${VST3SDK_TAG}.tar.gz
https://github.com/steinbergmedia/vst3_cmake/archive/refs/tags/v${VST3SDK_TAG}.tar.gz -> vst3sdk-cmake-${VST3SDK_TAG}.tar.gz
https://github.com/steinbergmedia/vst3_doc/archive/refs/tags/v${VST3SDK_TAG}.tar.gz -> vst3sdk-doc-${VST3SDK_TAG}.tar.gz
https://github.com/steinbergmedia/vst3_pluginterfaces/archive/refs/tags/v${VST3SDK_TAG}.tar.gz -> vst3sdk-plugininterfaces-${VST3SDK_TAG}.tar.gz
https://github.com/steinbergmedia/vst3_public_sdk/archive/refs/tags/v${VST3SDK_TAG}.tar.gz -> vst3sdk-public_sdk-${VST3SDK_TAG}.tar.gz
https://github.com/steinbergmedia/vst3_tutorials/archive/cb34a713bec1b96ac31a6bd66af607b92a4abb21.tar.gz
https://github.com/steinbergmedia/vstgui/archive/02eadc0d2d8295a3b856678fb67901c756580faf.tar.gz -> vstgui4-02eadc0d2d8295a3b856678fb67901c756580faf.tar.gz )
"

LICENSE="GPL-3"
SLOT="0/${PV}"
KEYWORDS="~amd64 ~arm64 ~x86"
IUSE="debug doc jack lv2 presets +standalone vst"

REQUIRED_USE="jack? ( standalone )"

DEPEND="
	dev-libs/libconfig
	dev-libs/rapidjson
	media-libs/libsndfile
	doc? ( virtual/pandoc )
	jack? ( virtual/jack )
	lv2? ( media-libs/lv2 )
	x11-libs/cairo
"

src_unpack() {
	default

	mv "${WORKDIR}/${PN}" "${S}" || die

    mv -T "${WORKDIR}/vst3sdk-${VST3SDK_TAG}" "${WORKDIR}/vst3sdk" || die
    mv -T "${WORKDIR}/vst3_base-${VST3SDK_TAG}" "${WORKDIR}/vst3sdk/base" || die
	mv -T "${WORKDIR}/vst3_cmake-${VST3SDK_TAG}" "${WORKDIR}/vst3sdk/cmake" || die
	mv -T "${WORKDIR}/vst3_doc-${VST3SDK_TAG}" "${WORKDIR}/vst3sdk/doc" || die
    mv -T "${WORKDIR}/vst3_pluginterfaces-${VST3SDK_TAG}" "${WORKDIR}/vst3sdk/pluginterfaces" || die
	mv -T "${WORKDIR}/vst3_public_sdk-${VST3SDK_TAG}" "${WORKDIR}/vst3sdk/public.sdk" || die
    mv -T "${WORKDIR}/vst3_tutorials-cb34a713bec1b96ac31a6bd66af607b92a4abb21" "${WORKDIR}/vst3sdk/tutorials" || die
    mv -T "${WORKDIR}/vstgui-02eadc0d2d8295a3b856678fb67901c756580faf" "${WORKDIR}/vst3sdk/vstgui4" || die
}

src_configure() {
	if use lv2 or use vst; then
		build_plugin=ON
	else
		build_plugin=OFF
	fi

	local mycmakeargs=(
		-DGKICK_DOCUMENTATION=$(usex doc ON OFF)
		-DGKICK_PLUGIN=${build_plugin}
		-DGKICK_PLUGIN_LV2=$(usex lv2 ON OFF)
		-DGKICK_PLUGIN_VST=$(usex vst ON OFF)
		-DVST3_SDK_PATH=${WORKDIR}/vst3sdk
		-DGKICK_PRESETS=$(usex presets ON OFF)
		-DGKICK_STANDALONE=$(usex standalone ON OFF)
		-DSMTG_ENABLE_VST3_PLUGIN_EXAMPLES=OFF
		-DSMTG_ENABLE_VST3_HOSTING_EXAMPLES=OFF
	)

	CMAKE_BUILD_TYPE=$(usex debug "Debug" "Release")

	cmake_src_configure
}

src_install() {
	cmake_src_install

	rm -rv "${D}/usr/bin/rkpng2c" || die
	rm -rv "${D}/usr/include" || die
	rm -rv "${D}/usr/lib" || die

	mv -v "${D}/usr/share/doc/geonkick/Geonkick_User_Guide.pdf" "${D}/usr/share/doc/${P}" || die
	mv -v "${D}/usr/share/doc/geonkick/Geonkick_User_Guide_HTML" "${D}/usr/share/doc/${P}" || die

	rm -rv "${D}/usr/share/doc/geonkick"
}

pkg_postinst() {
	xdg_icon_cache_update
	xdg_mimeinfo_database_update
	xdg_desktop_database_update
}

pkg_postrm() {
	xdg_icon_cache_update
	xdg_mimeinfo_database_update
	xdg_desktop_database_update
}
